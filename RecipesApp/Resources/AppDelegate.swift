//
//  AppDelegate.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions
                        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let wireframe = Searchwireframe()
        guard let MainViewController = SearchModule().configure(wireframe: wireframe) else { return false }
        
        let navigationController = UINavigationController(rootViewController: MainViewController)
        wireframe.parentNavigationController = navigationController
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
        return true
    }
}
