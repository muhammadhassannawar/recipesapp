//
//  UICollectionView.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit
// MARK: Register and dequeue Collection view cell
extension UICollectionView {
    func register<T: UICollectionViewCell>(type: T.Type) {
        register(UINib.init(nibName: T.nibName, bundle: nil),
                 forCellWithReuseIdentifier: T.identifier)
    }
    
    func cell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T
    }
}

// MARK: Get cell identifier and cell name
extension UICollectionReusableView {
    static var identifier: String { "\(self)" }
    class var nibName: String { "\(self)" }
}
