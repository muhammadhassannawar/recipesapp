//
//  HostService.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

struct HostService {
    // MARK: - static Properties And Methods
    static func getBaseURL() -> String {
        return "https://api.edamam.com/"
    }
    static var headers: [String: String] { [:] }
    static var extraParameters: [String: AnyObject] {
        ["app_key": "0b2d2f599ba7abc2454d6b7aeaa3cf43", "app_id": "771060f9"] as [String: AnyObject]
    }
}
