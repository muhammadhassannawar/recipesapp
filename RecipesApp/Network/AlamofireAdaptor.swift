//
//  AlamofireAdaptor.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit
import Alamofire

class AlamofireAdaptor: NetworkingInterface {
    let requestBaseURL: String
    let requestHeaders: [String: String]?
    let extraParameters: [String: AnyObject]?
    let validContentTypes = ["application/json",
                             "Accept", "application/hal+json", "text/html"]
    init(baseURL: String, headers: [String: String]? = nil, extraParameters: [String: AnyObject]?) {
        self.requestBaseURL = baseURL
        self.requestHeaders = headers
        self.extraParameters = extraParameters
    }
    
    func request<T: Decodable>(_ specs: RequestSpecs<T>,
                               completionBlock: @escaping (T?, Error?) -> Void) {
        let url = requestBaseURL + specs.urlString
        let encoding = convertRequestEnconding(specs.encoding)
        var parameters = (specs.parameters ?? [:])
        parameters.merge((extraParameters ?? [:]),
                         uniquingKeysWith: { value1, _ in
                            return value1 })
        let request = AF.request(url, method: convertRequestMethodToAlamofireMethod(specs.method),
                                 parameters: parameters, encoding: encoding,
                                 headers: HTTPHeaders(requestHeaders ?? [:]))
            .validate(contentType: self.validContentTypes)
        request.responseDecodable(of: T.self) { result in
            guard let data = result.value else {
                completionBlock(nil, result.error)
                return
            }
            completionBlock(data, nil)
        }
        
    }
    
    func convertRequestEnconding(_ enconding: Encoding) -> ParameterEncoding {
        switch enconding {
        case .json:
            return JSONEncoding.default
        case.urlEncodedInURL:
            return URLEncoding.default
        }
    }
    
    func convertRequestMethodToAlamofireMethod(_ method: RequestMethod) -> Alamofire.HTTPMethod {
        switch method {
        case .DELETE:
            return HTTPMethod.delete
        case .GET:
            return HTTPMethod.get
        case .PUT:
            return HTTPMethod.put
        case .POST:
            return HTTPMethod.post
        }
    }
}

struct NetworkState {
    var isInternetAvailable: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
