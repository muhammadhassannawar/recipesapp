//
//  RecipeDetailsViewController.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/6/21.
//

import UIKit
import SDWebImage

protocol RecipeDetailsView: AnyObject {
    func showRecipeDetails(_ recipe: Recipe)
}

class RecipeDetailsViewController: UIViewController {
    // MARK: - Properties And Outlets
    var presenter: RecipeDetailsPresenter?
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeIngredientsLabel: UILabel!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recipe Details"
        addShareButtonInNavigationBar()
        presenter?.viewDidLoad()
    }
    
    private func addShareButtonInNavigationBar() {
        let doneButton = UIBarButtonItem(image: #imageLiteral(resourceName: "share"),
                                         style: UIBarButtonItem.Style.plain,
                                         target: self, action: #selector(self.clickOnShareRecipe(sender:)))
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    @objc func clickOnShareRecipe(sender: UIBarButtonItem) {
        presenter?.didClickOnShareRecipe()
    }
    
    @IBAction func recipeWebsite(_ sender: Any) {
        presenter?.didClickOnRecipeWebsite()
    }
}

// MARK: - View Protocol
extension RecipeDetailsViewController: RecipeDetailsView {
    func showRecipeDetails(_ recipe: Recipe) {
        self.recipeTitleLabel.text = recipe.label
        self.recipeIngredientsLabel.text = recipe.ingredientLines.joined(separator: "\n\n")
        guard let url = URL(string: recipe.image) else { return }
        recipeImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        recipeImageView.sd_setImage(with: url)
    }
}
