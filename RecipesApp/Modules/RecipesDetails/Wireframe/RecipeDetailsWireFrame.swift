//
//  RecipeDetailswireframe.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/6/21.
//

import SafariServices
import UIKit

protocol RecipeDetailswireframeProtocol {
    func showRecipeWebsite(url: String)
    func showShareRecipe(url: String)
    func showRecipeDetailsScreen(in parentNavigationController: UINavigationController?, recipe: Recipe)
}

class RecipeDetailswireframe: RecipeDetailswireframeProtocol {
    var parentNavigationController: UINavigationController?
    func showRecipeDetailsScreen(in parentNavigationController: UINavigationController? = nil, recipe: Recipe) {
        guard let parentNavigationController = parentNavigationController ?? self.parentNavigationController,
              let controller = RecipeDetailsModule().configure(parentNavigationController: parentNavigationController, recipe: recipe) else { return }
        self.parentNavigationController = parentNavigationController
        self.parentNavigationController?.pushViewController(controller, animated: false)
    }
    
    func showShareRecipe(url: String) {
        if let link = NSURL(string: url) {
            let objectsToShare = [link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.parentNavigationController?.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func showRecipeWebsite(url: String) {
        if let url = URL(string: url) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            self.parentNavigationController?.present(vc, animated: true)
        }
    }
}
