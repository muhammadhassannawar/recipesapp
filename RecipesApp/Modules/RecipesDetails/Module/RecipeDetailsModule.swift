//
//  RecipeDetailsModule.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/6/21.
//

import UIKit

class RecipeDetailsModule {
    /// Configure the requirements and dependencies of this MainViewController
    /// - Parameter viewController: MainViewController
    func configure(parentNavigationController: UINavigationController? = nil, recipe: Recipe) -> RecipeDetailsViewController? {
        guard let viewController = RecipeDetailsViewController.loadFromNib() else { return nil }
        let wireframe = RecipeDetailswireframe()
        wireframe.parentNavigationController = parentNavigationController
        let presenter = RecipeDetailsPresenter(view: viewController, recipe: recipe, wireframe: wireframe)
        viewController.presenter = presenter
        return viewController
    }
}
