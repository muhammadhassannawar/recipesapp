//
//  RecipeDetailsPresenter.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/6/21.
//

import Foundation

protocol RecipeDetailsPresenterProtocol: AnyObject {
    func viewDidLoad()
    func didClickOnRecipeWebsite()
    func didClickOnShareRecipe()
}

class RecipeDetailsPresenter {
    // MARK: - Private Properties
    private let view: RecipeDetailsView
    private let wireframe: RecipeDetailswireframe
    private var recipe: Recipe
    private let isbnLimit = 5
    init(view: RecipeDetailsView, recipe: Recipe, wireframe: RecipeDetailswireframe) {
        self.view = view
        self.recipe = recipe
        self.wireframe = wireframe
    }
}

// MARK: - Presenter Protocol methods
extension RecipeDetailsPresenter: RecipeDetailsPresenterProtocol {
    func didClickOnRecipeWebsite() {
        wireframe.showRecipeWebsite(url: recipe.url)
    }
    
    func didClickOnShareRecipe() {
        wireframe.showShareRecipe(url: recipe.shareAs)
    }
    
    func viewDidLoad() {
        self.view.showRecipeDetails(recipe)
    }
}

