//
//  SearchModel.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import Foundation

struct SearchResult: Codable {
    var more: Bool = true
    var hits: [RecipeData]
}

struct RecipeData: Codable {
    var recipe: Recipe
}

struct Recipe: Codable {
    let label: String
    let image: String
    let source: String
    let healthLabels: [String]
    let ingredientLines: [String]
    let shareAs: String
    let url: String
}

/// Filter types for display
enum SearchType: String {
    case all, lowSugar, keto, vegan
    var identifier: String {
        switch self {
        case .all: return "ALL"
        case .lowSugar: return "Low Sugar"
        case .keto: return "Keto"
        case .vegan: return "Vegan"
        }
    }
    /// Filter types for api service
    var parameterIdentifier: String {
        switch self {
        case .all: return ""
        case .lowSugar: return "low-sugar"
        case .keto: return "keto-friendly"
        case .vegan: return "vegan"
        }
    }
}
