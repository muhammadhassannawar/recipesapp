//
//  SortedSuggestions.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import Foundation

class SortedSuggestions {
    // MARK: - Properties and methods to store, remove, update and check exist of Sorted Suggestions
    private static let suggestionsKey = "Suggestions"
    public static var shared = SortedSuggestions()
    private var storedSuggestions: [String]? {
        if let json = UserDefaults.standard.object(forKey: SortedSuggestions.suggestionsKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedData = try? decoder.decode([String].self, from: json) {
                return loadedData
            } else {
                return nil
            }
        }
        return nil
    }
    
    private func storeSuggestions(_ suggestions: [String]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(suggestions) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: SortedSuggestions.suggestionsKey)
        }
    }
    
    func loadSortedSuggestions(completion: @escaping ([String]?) -> Void) {
        DispatchQueue.global(qos: .utility).async {
            let suggestions = self.storedSuggestions
            DispatchQueue.main.async {
                completion(suggestions)
            }
        }
    }
    
    func addSuggestionToSortedSuggestions(name: String, completion: @escaping (String) -> Void) {
        DispatchQueue.global(qos: .utility).async {
            var suggestions = self.storedSuggestions ?? []
            if let index = suggestions.firstIndex(where: {$0 == name}) {
                suggestions.remove(at: index)
            }
            suggestions.insert(name, at: 0)
            if suggestions.count > 10 {
                suggestions.removeLast()
            }
            self.storeSuggestions(suggestions)
        }
    }
}
