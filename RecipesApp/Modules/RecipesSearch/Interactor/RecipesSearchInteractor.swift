//
//  RecipesRecipesSearchInteractor.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import Foundation

protocol RecipesSearchInteractorProtocol {
    func LoadRecipes(searchType: SearchType, startFrom: Int, searchText: String, completionHundler: @escaping (SearchResult?, String?) -> Void)
    func loadSortedSuggestions(completion: @escaping ([String]?) -> Void)
    func addSuggestionToSortedSuggestions(name: String, completion: @escaping (String) -> Void)
}

class RecipesSearchInteractor {
    private let coreNetwork: CoreNetworkProtocol
    private var entity = SortedSuggestions.shared
    
    init(coreNetwork: CoreNetworkProtocol, entity: SortedSuggestions) {
        self.coreNetwork = coreNetwork
        self.entity = entity
    }
}
// MARK: - Interactor Protocol methods
extension RecipesSearchInteractor: RecipesSearchInteractorProtocol {
    func addSuggestionToSortedSuggestions(name: String, completion: @escaping (String) -> Void) {
        entity.addSuggestionToSortedSuggestions(name: name, completion: completion)
    }
    
    func loadSortedSuggestions(completion: @escaping ([String]?) -> Void) {
        entity.loadSortedSuggestions(completion: completion)
    }
    
    /// load recipes from API
    /// - Parameters:
    ///   - searchText: Name we need to search for
    ///   - completionHundler: Return search result or an error in case fail request
    func LoadRecipes(searchType: SearchType, startFrom: Int, searchText: String, completionHundler: @escaping (SearchResult?, String?) -> Void) {
        var parameter = ["q": searchText, "from": startFrom] as [String: AnyObject]
        if searchType.parameterIdentifier != "" {
            parameter.updateValue(searchType.parameterIdentifier as AnyObject, forKey: "health")
        }
        let request = RequestSpecs<SearchResult>(method: .GET, urlString: "search", parameters: parameter)
        coreNetwork.makeRequest(request: request, completion: { model, error in
            completionHundler(model, error?.localizedDescription)
        })
    }
}
