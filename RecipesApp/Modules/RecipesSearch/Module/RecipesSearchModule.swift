//
//  RecipesSearchModule.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import Foundation

class SearchModule {
    /// Configure the requirements and dependencies of this RecipesSearchViewController
    /// - Parameter viewController: RecipesSearchViewController
    func configure(wireframe: Searchwireframe? = nil) -> RecipesSearchViewController? {
        let entity = SortedSuggestions.shared
        let coreNetwork = CoreNetwork()
        let interactor = RecipesSearchInteractor(coreNetwork: coreNetwork, entity: entity)
        guard let viewController = RecipesSearchViewController.loadFromNib() else { return nil }
        let presenter = RecipesSearchPresenter(
            view: viewController, interactor: interactor,
            searchType: SearchType.all, wireframe: wireframe ?? Searchwireframe())
        viewController.presenter = presenter
        return viewController
    }
}
