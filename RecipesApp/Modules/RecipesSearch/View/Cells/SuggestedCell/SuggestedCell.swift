//
//  SuggestedCell.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

protocol SuggestedCellView {
    func configure(_ suggestedName: String)
}

class SuggestedCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var suggestionLabel: UILabel!
}

// MARK: - View Protocol
extension SuggestedCell: SuggestedCellView {
    func configure(_ suggestedName: String) {
        suggestionLabel.text = suggestedName
    }
}
