//
//  RecipeCell.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit
import SDWebImage

protocol RecipeCellView {
    func configure(_ recipe: Recipe)
}

class RecipeCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var recipeHealthLabelsLabel: UILabel!
    @IBOutlet weak var recipeSourceLabel: UILabel!
    @IBOutlet weak var recipeTitleLabel: UILabel!
    @IBOutlet weak var recipeImageView: UIImageView!
    override func prepareForReuse() {
        super.prepareForReuse()
        recipeImageView.stopLoading()
    }
}

// MARK: - View Protocol
extension RecipeCell: RecipeCellView {
    func configure(_ recipe: Recipe) {
        recipeTitleLabel.text = recipe.label
        recipeSourceLabel.text = recipe.source
        recipeHealthLabelsLabel.text = recipe.healthLabels.joined(separator: ", ")
        guard let url = URL(string: recipe.image) else { return }
        recipeImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        recipeImageView.sd_setImage(with: url)
    }
}
