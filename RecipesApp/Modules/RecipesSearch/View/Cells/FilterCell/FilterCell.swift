//
//  FilterCell.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

protocol FilterCellView {
    func configure(_ title: String,_ selected: Bool)
}

class FilterCell: UICollectionViewCell {
    // MARK: - Outlets
    @IBOutlet weak var filterTitleView: UIView!
    @IBOutlet weak var filterTitleLabel: UILabel!
}

// MARK: - View Protocol
extension FilterCell: FilterCellView {
    func configure(_ title: String,_ selected: Bool) {
        filterTitleLabel.text = title
        if selected {
            filterTitleView.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        } else {
            filterTitleView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
}
