//
//  RecipesSearchViewController.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

protocol SearchView: AnyObject {
    func showMessage(_ message: String)
    func showSearchResults(with results: [RecipeData])
    func hideLoadingView()
    func showLoadingView()
    func showSuggestion()
    func hideSuggestion()
    func dismissSuggestionView()
    func dismissKeyboard()
    func removeSearchbarText()
    func updateSearchBarText(text: String)
    func updateSortedSuggestionsList(with sortedSuggestions: [String])
    func updateFilterList(with filters: [SearchType])
}

class RecipesSearchViewController: UIViewController {
    // MARK: - Properties And Outlets
    var presenter: RecipesSearchPresenter?
    private var recipes: [RecipeData] = []
    private var sortedSuggestions: [String] = []
    private let suggestionCellHeight = 37
    private let filtrCellHeight = 60.0
    private let filtrCellWidth = 150.0
    private var filters: [SearchType] = []
    private var selectedFilterIndex = 0
    @IBOutlet weak private var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var filterCollectionView: UICollectionView!{
        didSet {
            setupCollectionView()
        }
    }
    @IBOutlet weak private var searchTermsField: UISearchBar!
    @IBOutlet weak private var RecipesTableView: UITableView!{
        didSet {
            setupTableView(RecipesTableView, RecipeCell.self)
        }
    }
    @IBOutlet weak var suggestionsBackgroundView: UIView!
    @IBOutlet weak var suggestionTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var suggestionTableView: UITableView! {
        didSet {
            setupTableView(suggestionTableView, SuggestedCell.self)
        }
    }
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recipes Search"
        self.setupNavigationBar()
        setupSearchBar()
        handleTapOnSuggestionsBackgroundView()
        presenter?.viewDidLoad()
    }
    
    // MARK: - handle Suggestion List Height Based On The Suggestions Amount
    private func updateSuggestionTableViewHeight() {
        self.suggestionTableViewHeight.constant = CGFloat((self.sortedSuggestions.count * suggestionCellHeight))
        self.view.layoutIfNeeded()
        self.suggestionTableView.scroll(to: .top, animated: false)
        self.suggestionTableView.reloadData()
    }
    
    // MARK: - handle Tap On Background View To Dismiss Suggestions List View
    private func handleTapOnSuggestionsBackgroundView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapOnSuggestionsBackgroundView(_:)))
        suggestionsBackgroundView.addGestureRecognizer(tap)
    }
    
    @objc private func handleTapOnSuggestionsBackgroundView(_ sender: UITapGestureRecognizer? = nil) {
        presenter?.tapOnSuggestionsBackgroundView()
    }
}

// MARK: - UI Search Bar Setup And Delegate
extension RecipesSearchViewController: UISearchBarDelegate {
    private func setupSearchBar() {
        searchTermsField.delegate = self
        searchTermsField.barTintColor = UIColor.clear
        if #available(iOS 13.0, *) {
            searchTermsField.searchTextField.textColor = .black
        }
        searchTermsField.backgroundColor = UIColor.clear
        searchTermsField.isTranslucent = true
        searchTermsField.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchTermsField.showsCancelButton = true
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .done,
                                         target: self, action: #selector(clickCancelButtonOnKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        searchTermsField.inputAccessoryView = toolbar
    }
    
    @objc func clickCancelButtonOnKeyboard() {
        presenter?.didClickKeyboardCancelButton()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        presenter?.didPressSearch(searchText: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.searchBarCancelButtonClicked()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if (searchBar.text == "") {
            presenter?.searchBarBeginEditing()
        }
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            presenter?.searchBarBeginEditing()
        }else {
            presenter?.searchBarTextDidChange(searchText: searchText)
        }
    }
}

// MARK: - View Protocol
extension RecipesSearchViewController: SearchView {
    func updateFilterList(with filters: [SearchType]) {
        self.filters = filters
    }
    
    func hideSuggestion() {
        self.suggestionsBackgroundView.isHidden = true
        self.suggestionTableView.isHidden = true
    }
    
    func dismissSuggestionView() {
        self.suggestionsBackgroundView.isHidden = true
        self.suggestionTableView.isHidden = true
        self.searchTermsField.resignFirstResponder()
    }
    
    func showSuggestion() {
        self.suggestionsBackgroundView.isHidden = false
        self.suggestionTableView.isHidden = false
        self.updateSuggestionTableViewHeight()
    }
    
    func updateSortedSuggestionsList(with sortedSuggestions: [String]) {
        self.sortedSuggestions = sortedSuggestions
        self.updateSuggestionTableViewHeight()
    }
    
    func updateSearchBarText(text: String) {
        self.searchTermsField.text = text
    }
    
    func dismissKeyboard() {
        self.searchTermsField.resignFirstResponder()
    }
    
    func removeSearchbarText() {
        self.searchTermsField.text = ""
    }
    
    func showMessage(_ message: String) {
        self.showMessageAlert(message)
    }
    
    func hideLoadingView() {
        loadingView.stopAnimating()
    }
    
    func showLoadingView() {
        loadingView.startAnimating()
    }
    
    func showSearchResults(with results: [RecipeData]) {
        recipes = results
        self.RecipesTableView.reloadData()
    }
}

// MARK: - UITableView Data Source And Delegate
extension RecipesSearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case RecipesTableView:
            return recipes.count
        case suggestionTableView:
            return sortedSuggestions.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case RecipesTableView:
            return getConfiguredRecipeCell(tableView: tableView, for: indexPath)
        case suggestionTableView:
            return getConfiguredSortedSuggestionCell(tableView: tableView, for: indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch tableView {
        case RecipesTableView:
            if indexPath.row == (recipes.count) - 1 {
                presenter?.didScrollToLast()
            }
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case RecipesTableView:
            presenter?.didSelectRowInRecipesTable(index: indexPath.row)
        case suggestionTableView:
            presenter?.didSelectRowInSortedSuggestionsTable(index: indexPath.row)
        default:
            return
        }
    }
    
    private func getConfiguredRecipeCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let recipe = recipes[index.row].recipe
        let cell: RecipeCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(recipe)
        return cell ?? UITableViewCell()
    }
    
    private func getConfiguredSortedSuggestionCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let suggestion = sortedSuggestions[index.row]
        let cell: SuggestedCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(suggestion)
        return cell ?? UITableViewCell()
    }
    
    private func setupTableView(_ tableView: UITableView,_ cellType: UITableViewCell.Type) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.tableFooterView = UIView()
        tableView.register(cellType)
    }
}

// MARK: - UICollectionView Data Source And Delegate
extension RecipesSearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: FilterCell = collectionView.cell(for: indexPath) else { return UICollectionViewCell() }
        let selected = indexPath.item == selectedFilterIndex ? true : false
        cell.configure(filters[indexPath.item].identifier, selected)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedFilterIndex = indexPath.item
        let newIndex = IndexPath(row: selectedFilterIndex, section: 0)
        filterCollectionView.scrollToItem(at: newIndex, at: UICollectionView.ScrollPosition.left, animated: true)
        filterCollectionView.reloadData()
        presenter?.didSelectFilter(searchType: filters[indexPath.item], searchText: searchTermsField.text ?? "")
    }
    
    private func setupCollectionView() {
        filterCollectionView.delegate = self
        filterCollectionView.dataSource = self
        filterCollectionView.backgroundColor = UIColor.clear
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.filterCollectionView.collectionViewLayout = layout
        filterCollectionView.register(type: FilterCell.self)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RecipesSearchViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: filtrCellWidth, height: filtrCellHeight)
    }
}
