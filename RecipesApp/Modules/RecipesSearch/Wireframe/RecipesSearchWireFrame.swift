//
//  RecipesSearchwireframe.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import UIKit

protocol SearchwireframeProtocol {
    func showRecipeDetails(recipe: Recipe)
}

// MARK: - Search wireframe Protocol method to navigate to recipe details
class Searchwireframe: SearchwireframeProtocol {
    var parentNavigationController: UINavigationController?
    func showRecipeDetails(recipe: Recipe) {
        RecipeDetailswireframe().showRecipeDetailsScreen(in: parentNavigationController, recipe: recipe)
    }
}
