//
//  RecipesSearchPresenter.swift
//  RecipesApp
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import Foundation

protocol SearchPresenterProtocol: AnyObject {
    func didPressSearch(searchText: String)
    func didClickKeyboardCancelButton()
    func searchBarCancelButtonClicked()
    func searchBarBeginEditing()
    func searchBarTextDidChange(searchText: String)
    func didScrollToLast()
    func viewDidLoad()
    func tapOnSuggestionsBackgroundView()
    func didSelectRowInRecipesTable(index: Int)
    func didSelectRowInSortedSuggestionsTable(index: Int)
    func didSelectFilter(searchType: SearchType, searchText: String)
    func searchBySpecificText(text: String)
}

class RecipesSearchPresenter {
    // MARK: - Private Properties
    private let view: SearchView
    private let interactor: RecipesSearchInteractor?
    private let wireframe: Searchwireframe
    private var isLoading = false
    private var areThereMoreRecipes = true
    private var startFrom = 0
    private var searchText = ""
    private var recipesModels: [RecipeData] = []
    private var sortedSuggestions: [String] = []
    private var filters: [SearchType] = [SearchType.all, SearchType.lowSugar,
                                         SearchType.keto, SearchType.vegan]
    private var selectedFilterType: SearchType
    
    init(view: SearchView, interactor: RecipesSearchInteractor?, searchType: SearchType, wireframe: Searchwireframe) {
        self.view = view
        self.interactor = interactor
        self.selectedFilterType = searchType
        self.wireframe = wireframe
    }
    
    // MARK: - Private methods
    private func LoadRecipes(searchType: SearchType, searchText: String, startFrom: Int, completion: (()->Void)? = nil) {
        guard !searchText.isEmpty else {
            self.view.hideLoadingView()
            setEmptyMessage("There is no keyword to search for")
            return
        }
        isLoading = true
        interactor?.LoadRecipes(searchType: searchType, startFrom: startFrom, searchText: searchText) { [weak self] (result, error) in
            completion?()
            self?.isLoading = false
            if let error = error {
                DispatchQueue.main.async {
                    self?.view.showMessage(error)
                }
            } else if let result = result {
                self?.areThereMoreRecipes = result.more
                self?.didLoadRecipes(models: result.hits, startFrom: startFrom)
            }
        }
    }
    
    private func addSuggestionToSortedSuggestions(name: String) {
        interactor?.addSuggestionToSortedSuggestions(name: name) { [weak self] _ in
            self?.loadSortedSuggestions()
        }
    }
    
    private func validateSearchText(_ searchText: String) {
        if searchText == " " {
            self.view.updateSearchBarText(text: "")
            self.view.showMessage("empty spaces are not allowed")
        }
        let regex = try! NSRegularExpression(pattern: "[a-zA-Z\\s]+", options: [])
        let range = regex.rangeOfFirstMatch(in: searchText, options: [], range: NSRange(location: 0, length: searchText.count))
        if range.length != searchText.count {
            self.view.showMessage("only english letters & spaces are allowed to be inserted in the search")
        }
    }
    
    private func showRecipeDetails(index: Int) {
        let recipe = recipesModels[index].recipe
        wireframe.showRecipeDetails(recipe: recipe)
    }
    
    private func loadSortedSuggestions() {
        interactor?.loadSortedSuggestions { [weak self] suggestions in
            guard let suggestions = suggestions else { return }
            self?.sortedSuggestions = suggestions
            self?.view.updateSortedSuggestionsList(with: suggestions)
        }
    }
    
    private func didLoadRecipes(models: [RecipeData], startFrom: Int) {
        if startFrom == 0 {
            didFirstTimeLoadRecipes(models: models)
        } else {
            if models.isEmpty && areThereMoreRecipes == false {
                areThereMoreRecipes = false
            }else{
                self.recipesModels.append(contentsOf: models)
                areThereMoreRecipes = true
            }
        }
        view.showSearchResults(with: recipesModels)
    }
    
    private func didFirstTimeLoadRecipes(models: [RecipeData]) {
        guard !models.isEmpty else {
            setEmptyMessage("No search results for \(searchText) in \(selectedFilterType.identifier) categories")
            return
        }
        addSuggestionToSortedSuggestions(name: searchText)
        loadSortedSuggestions()
        self.recipesModels = models
    }
    
    private func setEmptyMessage(_ message: String) {
        self.recipesModels = []
        self.view.showSearchResults(with: recipesModels)
        self.view.showMessage(message)
    }
}

// MARK: - Presenter Protocol methods
extension RecipesSearchPresenter: SearchPresenterProtocol {
    func tapOnSuggestionsBackgroundView() {
        self.view.dismissSuggestionView()
    }
    
    func viewDidLoad() {
        loadSortedSuggestions()
        self.view.updateFilterList(with: self.filters)
    }
    
    func searchBarTextDidChange(searchText: String) {
        self.view.hideSuggestion()
        self.searchText = searchText
        validateSearchText(searchText)
    }
    
    func searchBarBeginEditing() {
        loadSortedSuggestions()
        self.view.showSuggestion()
    }
    func didSelectRowInSortedSuggestionsTable(index: Int) {
        searchBySpecificText(text: sortedSuggestions[index])
        self.view.hideSuggestion()
    }
    func searchBySpecificText(text: String) {
        self.view.updateSearchBarText(text: text)
        self.searchText = text
        guard !isLoading else { return }
        view.showLoadingView()
        startFrom = 0
        LoadRecipes(searchType: selectedFilterType, searchText: searchText, startFrom: startFrom) {
            self.view.hideLoadingView()
        }
    }
    
    func didSelectFilter(searchType: SearchType, searchText: String) {
        self.searchText = searchText
        guard !isLoading else { return }
        areThereMoreRecipes = true
        self.selectedFilterType = searchType
        view.showLoadingView()
        startFrom = 0
        LoadRecipes(searchType: selectedFilterType, searchText: searchText, startFrom: startFrom) {
            self.view.hideLoadingView()
        }
    }
    
    func didClickKeyboardCancelButton() {
        cancelSearch()
    }
    
    func didPressSearch(searchText: String) {
        self.searchText = searchText
        self.startFrom = 0
        self.view.dismissKeyboard()
        guard !isLoading else { return }
        view.showLoadingView()
        LoadRecipes(searchType: selectedFilterType, searchText: searchText, startFrom: startFrom) {
            self.view.hideLoadingView()
        }
    }
    
    func searchBarCancelButtonClicked() {
        cancelSearch()
    }
    
    func didScrollToLast() {
        // Check if no loading recipes request fetch or if we not reach last elelment in search
        guard !isLoading && areThereMoreRecipes else { return }
        view.showLoadingView()
        self.startFrom = startFrom + 10
        LoadRecipes(searchType: selectedFilterType, searchText: searchText, startFrom: self.startFrom) {
            self.view.hideLoadingView()
        }
    }
    
    func cancelSearch() {
        self.view.dismissKeyboard()
        self.view.removeSearchbarText()
    }
    
    func didSelectRowInRecipesTable(index: Int) {
        self.showRecipeDetails(index: index)
    }
}
