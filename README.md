# RecipesApp

### Overview

This repo includes a sample project that demonstrates the VIPER architecture patterns on iOS.

* Xcode 12.5
* Swift 5

in this project The main screen which contain a list of recipes, this list populated by typing search query and this search quary is stored as suggested quary in the future (suggest last ten queries) in case the query has result , when click in one of this recipes get recipe details which contain recipe ingredients and can share the recipe with others and can open the website of the recipe to see all details.

<img src="https://i.postimg.cc/HsmL1jxF/Simulator-Screen-Shot-i-Phone-12-Pro-Max-2021-12-08-at-02-03-16.png" width="250"> <img src="https://i.postimg.cc/sgNyx2jj/Simulator-Screen-Shot-i-Phone-12-Pro-Max-2021-12-08-at-02-03-26.png" width="250">

# Pods
In this project includes some of useful pods for iOS, such as:
  * pod 'SDWebImage'
  * pod 'Alamofire'
  * pod 'Mockit'

# Included API

This project requires the following API:
* Search for recipes by name (https://api.edamam.com/search?app_id=&app_key=&from=&health=&q=)
