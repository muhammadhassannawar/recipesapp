//
//  TestSearchModule.swift
//  TestSearchModule
//
//  Created by Muhammad Hassan Nawar on 12/4/21.
//

import XCTest
import Foundation
import Mockit

class TestSearchModule: XCTestCase {
    var coreNetwork: MockCoreNetwork<SearchResult>!
    var presenter: MockSearchPresenter!
    var interator: RecipesSearchInteractor!
    var view: MockSearchView!
    var recipes: [RecipeData] = []
    var entity = SortedSuggestions()
    let wireframe = Searchwireframe()
    var expextedCount: Int = 0
    let requiredInputErrorMessage = "Please provide a valid value for search term " +
        "and select one ore more category"
    
    override func setUpWithError() throws {
        expextedCount = 0
        testMangingRecipesLogic()
        view = MockSearchView.init(testCase: self)
        coreNetwork = MockCoreNetwork<SearchResult>()
        interator = RecipesSearchInteractor(coreNetwork: coreNetwork, entity: entity)
        presenter = MockSearchPresenter(view: view, interactor: interator, wireframe: wireframe, testCase: self)
    }
    
    func testMangingRecipesLogic() {
        let result = [RecipeData]()
        assert(result.allSatisfy({ resultSection in
            recipes.contains(where: { expected in
                resultSection.recipe.label == expected.recipe.label
            })
        }))
    }
    
    func testDidLoadRecipes() throws {
        self.presenter.didScrollToLast()
        view.showSearchResults(with: recipes)
        wait(for: 3)
    }
    
    func testdDidLoadSearchResults() throws {
        let result = SearchResult.init(more: true, hits: recipes)
        coreNetwork?.result = result
        coreNetwork?.error = nil
        view?.verify(verificationMode: AtMostOnce())
            .showLoadingView()
        interator?.LoadRecipes(searchType: .all, startFrom: 0, searchText: "Al") { [weak self]
            result, error in
            if error != nil {
            }else{
                guard let recipes = result?.hits else { return }
                self?.recipes = recipes
                
            }
        }
        view?.verify(verificationMode: AtMostOnce())
            .hideLoadingView()
        wait(for: 3)
    }
    
    func testEmptyRecipes() throws {
        view?.verify(verificationMode: AtMostOnce())
            .showLoadingView()
        interator?.LoadRecipes(searchType: .all, startFrom: 0, searchText: "") { [weak self]
            result, error in
            if error == nil {
                self?.view?.verify(verificationMode: AtMostOnce()).showMessage(error ?? "")
            }else{
                guard let recipes = result?.hits else { return }
                self?.recipes = recipes
            }
        }
        view?.verify(verificationMode: AtMostOnce())
            .hideLoadingView()
    }
    
    func testFailedSelectFilter() throws {
        coreNetwork?.result = nil
        coreNetwork?.error = NSError(domain: "", code: 400,
                                     userInfo: [ NSLocalizedDescriptionKey: "Failed To Load Data"])
        presenter?.searchBySpecificText(text: "")
        wait(for: 3)
        view?.verify(verificationMode: Once()).hideLoadingView()
        view?.verify(verificationMode: Once()).showMessage("Failed To Load Data")
        presenter?.verify(verificationMode: Once()).didSelectFilter(searchType: .all, searchText: "")
    }
    
    func testViewController() throws {
        guard let viewController = RecipesSearchViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let parent = UINavigationController.init(rootViewController: viewController)
        viewController.showLoadingView()
        viewController.hideLoadingView()
        viewController.viewWillAppear(false)
        viewController.viewWillDisappear(false)
        assert(!parent.isNavigationBarHidden)
        viewController.presenter = presenter
        viewController.updateSearchBarText(text: "as")
        wait(for: 3)
        presenter.verify(verificationMode: Once())
            .viewDidLoad()
    }
}

class MockSearchView: SearchView, Mock {
    func showSearchResults(with results: [RecipeData]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: results)
    }
    
    func showSuggestion() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func hideSuggestion() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func dismissSuggestionView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func updateSortedSuggestionsList(with sortedSuggestions: [String]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: sortedSuggestions)
    }
    
    func updateFilterList(with filters: [SearchType]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: filters)
    }
    
    func showMessage(_ message: String) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: message)
    }
    
    func showLoadingView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func dismissKeyboard() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func removeSearchbarText() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func updateSearchBarText(text: String) {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: text)
    }
    
    func hideLoadingView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockSearchView {
        return self
    }
}

class MockCoreNetwork<Result: Codable>: CoreNetworkProtocol {
    var result: Result?
    var error: Error?

    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        if let result = self.result as? T {
            completion(result, error)
        } else {
            completion(nil, self.error)
        }
    }
}

extension XCTestCase {
    func wait(for seconds: Double) {
        let expectation = XCTestExpectation.init()
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: (seconds + 2))
    }
}

class MockSearchPresenter: RecipesSearchPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: SearchView, interactor: RecipesSearchInteractor, wireframe: Searchwireframe, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view, interactor: interactor, searchType: .all, wireframe: wireframe)
    }
    
    func instanceType() -> MockSearchPresenter {
        return self
    }
}
