//
//  TestRecipeDetailsModule.swift
//  RecipesAppTests
//
//  Created by Muhammad Hassan Nawar on 12/8/21.
//

import XCTest
import Foundation
import Mockit

class TestRecipeDetailsModule: XCTestCase {
    var presenter: MockRecipeDetailsPresenter!
    var view: MockRecipeDetailsView!
    var recipe = Recipe(label: "Apple", image: "", source: "Apple", healthLabels: ["Apple", "Apple"], ingredientLines: ["Apple", "Apple"], shareAs: "Share", url: "Url")
    let wireframe = RecipeDetailswireframe()
    override func setUpWithError() throws {
        view = MockRecipeDetailsView.init(testCase: self)
        presenter = MockRecipeDetailsPresenter(view: view, recipe: recipe, testCase: self)
    }
    
    func testViewDidLoad() throws {
        let presenter = RecipeDetailsPresenter(view: view, recipe: recipe, wireframe: RecipeDetailswireframe())
        presenter.viewDidLoad()
    }
    
    func testViewController() throws {
        guard let viewController = RecipeDetailsViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let parent = UINavigationController.init(rootViewController: viewController)
        viewController.viewWillAppear(false)
        viewController.viewWillDisappear(false)
        assert(!parent.isNavigationBarHidden)
        viewController.presenter = presenter
        wait(for: 3)
    }
}

class MockRecipeDetailsView: RecipeDetailsView, Mock {
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func showRecipeDetails(_ recipe: Recipe) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: recipe)
    }
    
    func instanceType() -> RecipeDetailsView {
        return self
    }
}

class MockRecipeDetailsPresenter: RecipeDetailsPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: MockRecipeDetailsView, recipe: Recipe, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view, recipe: recipe, wireframe: RecipeDetailswireframe())
    }
    
    func instanceType() -> RecipeDetailsPresenter {
        return self
    }
}
